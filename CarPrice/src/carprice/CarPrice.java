package carprice;

import java.nio.file.Path;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import javax.xml.ws.Response;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

public class CarPrice {
    
    @RequestMapping(value="/carprice",
                    method = RequestMethod.GET, 
                    consumes="application/json")
        public Response Carprice(@PathVariable String carManufacturer, @PathVariable String model, @Context UriInfo uriInfo) {
            
            for (Manufacturer m : Manufacturer.values()){
                if (!m.equals(carManufacturer))
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        
            for (Model m : Model.values()){
            if (!m.equals(model))
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            
            Random rn = new Random();
            return Response.created(uriInfo.getAbsolutePathBuilder()
                                       .path(String.valueOf(rn))
                                       .build());
        }
    
    public enum Manufacturer {
        Fiat,
        Ford,
        Toyota
    }
    
    public enum Model {
        Punto,
        Linea,
        Ecosport,
        Figo,
        Etios,
        Fortuner,
        Altis
    }
}
