package fordcar;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;

public class app {
    @RequestMapping(value="/carspecification",
                    method = RequestMethod.GET, 
                    consumes="application/json")
        public Response Carprice(@PathVariable String model, @Context UriInfo uriInfo) {
            
            Map<String,String> specification = new HashMap<String,String>();
        
            for (Model m : Model.values()){
            if (!m.equals(model))
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            
            if (model == "Ecosport") {
                ecosport ecosport = new ecosport();
                specification = ecosport.modelSpecification();
            }
            else if (model == "Figo") {
                figo figo = new figo();
                specification = figo.modelSpecification();
            }
            else if (model == "Mondeo") {
                mondeo mondeo = new mondeo();
                specification = mondeo.modelSpecification();
            }
            else if (model == "Endeavour") {
                endeavour endeavour = new endeavour();
                specification = endeavour.modelSpecification();
            }
            return Response.created(uriInfo.getAbsolutePathBuilder()
                                       .path(String.valueOf(specification))
                                       .build());
        }
    
    public enum Model {
        Ecosport,
        Figo,
        Monteo,
        Endeavour
    }
}
