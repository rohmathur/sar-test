package fordcar;

import fordcarfactory.car;

import java.util.HashMap;
import java.util.Map;

public class ecosport implements car {
      @Override
       public Map<String, String> modelSpecification() {
           Map<String,String> specification = new HashMap<String,String>();
           specification.put("SAR", "ecosport");
           return specification;
       }
}
